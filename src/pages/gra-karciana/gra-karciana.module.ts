import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GraKarcianaPage } from './gra-karciana';

@NgModule({
    declarations: [
        GraKarcianaPage
    ],
    imports: [
        IonicPageModule.forChild(GraKarcianaPage)
    ],
    entryComponents: [
        GraKarcianaPage
    ]
})
export class GraKarcianaPageModule {}
