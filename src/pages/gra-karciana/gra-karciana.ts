import { Component } from '@angular/core';
import { IonicPage, NavParams, MenuController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { GameControllerProvider } from '../../providers/game-controller/game-controller';

@IonicPage()
@Component({
  templateUrl: 'gra-karciana.html'
})
export class GraKarcianaPage {
    key: string;

    constructor(public navParams: NavParams, private menuCtrl: MenuController,
                public gameCtrl: GameControllerProvider,
                public storage: Storage) {
        this.key = navParams.get('key');
        if (this.key) {
            storage.set('schnapsenVisited', true);
            menuCtrl.enable(false, 'standard');
            menuCtrl.enable(true, 'schnapsen');
            return;
        }
    }

    doneClicked() {
         this.gameCtrl.navigateNextScreen(this.key);
    }
}

