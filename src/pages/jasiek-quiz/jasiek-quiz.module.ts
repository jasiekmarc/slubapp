import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { JasiekQuizPage } from './jasiek-quiz';
import { VerdictCardModule } from '../../components/verdict/verdict.module';

@NgModule({
    declarations: [
        JasiekQuizPage,
    ],
    imports: [
        IonicPageModule.forChild(JasiekQuizPage),
        VerdictCardModule,
    ],
    entryComponents: [
        JasiekQuizPage
    ]
})
export class JasiekQuizPageModule {}
