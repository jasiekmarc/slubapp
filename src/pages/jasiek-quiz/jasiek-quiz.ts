import { Component } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { IonicPage, ToastController, NavParams } from 'ionic-angular';
import { GameControllerProvider } from '../../providers/game-controller/game-controller';

@IonicPage()
@Component({
  templateUrl: 'jasiek-quiz.html'
})
export class JasiekQuizPage {
    quizFormGroup: FormGroup;
    question: string;
    hint: string;
    key: string;

    constructor(public formBuilder: FormBuilder, public navParams: NavParams,
                public toastCtrl: ToastController,
                public gameCtrl: GameControllerProvider) {
        this.key = navParams.get('key');
        this.quizFormGroup = formBuilder.group({
            p1: ['', Validators.required],
            p2: ['', Validators.required],
            p3: ['', Validators.required],
            p4: ['', Validators.required],
            p5: ['', Validators.required],
        });
        if (!this.key) {
            gameCtrl.navigateCurrentScreen();
        }
    }

    showHint() {
        let toast = this.toastCtrl.create({
            message: this.hint,
            duration: 3000
        });
        toast.present();
    }

    solvedPuzzles() {
        if (this.quizFormGroup.valid) {
            this.gameCtrl.navigateNextScreen(this.key);
            return;
        }
    }
}