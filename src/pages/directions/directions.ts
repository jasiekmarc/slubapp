import { Component } from '@angular/core';
import { IonicPage, NavParams, ToastController } from 'ionic-angular';
import { GameControllerProvider } from '../../providers/game-controller/game-controller';

@IonicPage()
@Component({
  templateUrl: 'directions.html'
})
export class DirectionsPage {
    question: string;
    hint: string;
    key: string;

    constructor(public navParams: NavParams, public toastCtrl: ToastController,
                public gameCtrl: GameControllerProvider) {
        this.question = navParams.get('question');
        this.hint = navParams.get('hint');
        this.key = navParams.get('key');
        if (!this.key) {
            gameCtrl.navigateCurrentScreen();
            return;
        }
    }

    showHint() {
        let toast = this.toastCtrl.create({
            message: this.hint,
            duration: 3000
        });
        toast.present();
    }

    pointReached() {
        this.gameCtrl.navigateNextScreen(this.key);
    }
}
