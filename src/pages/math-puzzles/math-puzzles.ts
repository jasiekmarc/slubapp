import { Component } from '@angular/core';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { IonicPage, ToastController, NavParams } from 'ionic-angular';
import { GameControllerProvider } from '../../providers/game-controller/game-controller';

@IonicPage()
@Component({
  templateUrl: 'math-puzzles.html'
})
export class MathPuzzlesPage {
    puzzlesFormGroup: FormGroup;
    question: string;
    hint: string;
    key: string;

    constructor(public formBuilder: FormBuilder, public navParams: NavParams,
                public toastCtrl: ToastController,
                public gameCtrl: GameControllerProvider) {
        this.key = navParams.get('key');
        this.puzzlesFormGroup = formBuilder.group({
            p1: ['', Validators.required],
            p2: ['', Validators.required],
            p3: ['', Validators.required],
            p4: ['', Validators.required],
            p5: ['', Validators.required],
        });
        if (!this.key) {
            gameCtrl.navigateCurrentScreen();
        }
    }

    showHint() {
        let toast = this.toastCtrl.create({
            message: this.hint,
            duration: 3000
        });
        toast.present();
    }

    solvedPuzzles() {
        let correct: number = 0;
        correct += (this.puzzlesFormGroup.get('p1').value == 'so') ? 1 : 0;
        correct += (this.puzzlesFormGroup.get('p2').value == 'on') ? 1 : 0;
        correct += (this.puzzlesFormGroup.get('p3').value == 'be') ? 1 : 0;
        correct += (this.puzzlesFormGroup.get('p4').value == 'o3') ? 1 : 0;
        correct += (this.puzzlesFormGroup.get('p5').value == 'ta') ? 1 : 0;

        if (correct == 5) {
            this.gameCtrl.navigateNextScreen(this.key);
            return;
        }

        let ending = 'ych';
        let are = 'jest';
        if (correct == 1) {
            ending = 'a';
        } else if (correct > 1 && correct < 5) {
            are = 'są';
            ending = 'e';
        }
        let toast = this.toastCtrl.create({
            message: `Spośród waszych odpowiedzi ${correct} ${are} poprawn${ending}.`,
            duration: 3000
        });
        toast.present();
    }
}
