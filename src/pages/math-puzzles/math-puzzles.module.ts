import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MathPuzzlesPage } from './math-puzzles';

@NgModule({
    declarations: [
        MathPuzzlesPage
    ],
    imports: [
        IonicPageModule.forChild(MathPuzzlesPage)
    ],
    entryComponents: [
        MathPuzzlesPage
    ]
})
export class MathPuzzlesPageModule {}
