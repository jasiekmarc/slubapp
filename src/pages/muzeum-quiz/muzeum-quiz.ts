import { Component } from '@angular/core';
import { Validators, FormBuilder, FormGroup, ValidatorFn, AbstractControl } from '@angular/forms';
import { IonicPage, NavParams } from 'ionic-angular';
import { GameControllerProvider } from '../../providers/game-controller/game-controller';

@IonicPage()
@Component({
  templateUrl: 'muzeum-quiz.html'
})
export class MuzeumQuizPage {
    puzzlesFormGroup: FormGroup;
    key: string;

    constructor(public formBuilder: FormBuilder, public navParams: NavParams,
                public gameCtrl: GameControllerProvider) {
        this.key = navParams.get('key');
        this.puzzlesFormGroup = formBuilder.group({
            p1: ['', Validators.compose([Validators.required, Validators.pattern(/Cec[iy]lia/i)])],
            p2: ['', Validators.compose([Validators.required, Validators.pattern(/Adam (Jerzy )?Czartoryski/i)])],
            p3: ['', Validators.compose([Validators.required, Validators.pattern(/Leonard D'?awinci/i)])],
            p41: ['', Validators.compose([Validators.required, offByMultiplicative(54, 0.15)])],
            p42: ['', Validators.compose([Validators.required, offByMultiplicative(39, 0.15)])],
            p5: ['', Validators.compose([Validators.required, between(1483, 1495)])],
        });
        if (!this.key) {
            gameCtrl.navigateCurrentScreen();
        }
    }

    solvedPuzzles() {
        if (this.puzzlesFormGroup.valid) {
            this.gameCtrl.navigateNextScreen(this.key);
        }
    }
}

function offByMultiplicative(val: number, slack: number): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} => {
    const given = control.value;
    const min = (1 - slack) * val;
    const max = (1 + slack) * val;
    const yes = min <= given && given <= max;
    return yes ? null : {'offByMultiplicative': {given}};
  };
}

function between(min: number, max: number): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} => {
        const inp = control.value;
        const yes = min <= inp && inp <= max;
        return yes ? null : {'between': {inp}};
    };
}
