import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MuzeumQuizPage } from './muzeum-quiz';

@NgModule({
    declarations: [
        MuzeumQuizPage
    ],
    imports: [
        IonicPageModule.forChild(MuzeumQuizPage)
    ],
    entryComponents: [
        MuzeumQuizPage
    ]
})
export class MuzeumQuizPageModule {}
