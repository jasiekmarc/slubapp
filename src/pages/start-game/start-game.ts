import { Component, ViewChild } from '@angular/core';
import { NavController, ToastController, Slides, AlertController, MenuController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { GameControllerProvider } from '../../providers/game-controller/game-controller';

@Component({
    templateUrl: 'start-game.html'
})
export class StartGamePage {
    @ViewChild(Slides) slides: Slides;
    teams = ["goat", "donkey", "pig", "dog", "cat", "duck"];

    swcompatible: boolean;
    swactive: boolean;

    constructor(public navCtrl: NavController, public menuCtrl: MenuController,
                public alertCtrl: AlertController, public storage: Storage,
                public toastController: ToastController,
                public gameCtrl: GameControllerProvider) {
        this.swcompatible = false;
        this.swactive = false;
        if (navigator.serviceWorker.ready) {
            this.swcompatible = true;
            navigator.serviceWorker.ready.then(() => {
                this.swactive = true;
            });
        }
    }

    selectTeam(team) {
        this.storage.remove('schnapsenVisited');
        this.menuCtrl.enable(true, 'standard');
        this.menuCtrl.enable(false, 'schnapsen');
        this.gameCtrl.team = team;
        setTimeout(() => {this.slides.slideNext();}, 1500);
    }

    startGameButtonClicked() {
        let alert = this.alertCtrl.create({
            title: 'Hasło',
            message: 'Proszę wpisać hasło rzucone na początku gry.',
            inputs: [{
                    type: 'text',
                    name: 'pass',
                    placeholder: 'Hasło',
                },
            ],
            buttons: [{
                    text: 'Wycofaj się',
                    role: 'cancel',
                    handler: data => {
                        console.log('Cancel clicked');
                    }
                }, {
                    text: 'Sprawdź',
                    handler: data => {
                        if (data.pass.match(/rze[zż]ucha/i)) {
                            this.gameCtrl.navigateNextScreen();
                        } else {
                            // invalid password
                            return false;
                        }
                    }
                }
            ]
        });
        alert.present();
    }

    continueGameButtonClicked() {
        this.gameCtrl.navigateCurrentScreen();
    }
}