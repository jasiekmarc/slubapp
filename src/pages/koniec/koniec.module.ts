import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KoniecPage } from './koniec';

@NgModule({
    declarations: [
        KoniecPage
    ],
    imports: [
        IonicPageModule.forChild(KoniecPage)
    ],
    entryComponents: [
        KoniecPage
    ]
})
export class KoniecPageModule {}
