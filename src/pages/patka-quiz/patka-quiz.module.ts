import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PatkaQuizPage } from './patka-quiz';
import { VerdictCardModule } from "../../components/verdict/verdict.module";

@NgModule({
    declarations: [
        PatkaQuizPage,
    ],
    imports: [
        IonicPageModule.forChild(PatkaQuizPage),
        VerdictCardModule,
    ],
    entryComponents: [
        PatkaQuizPage
    ]
})
export class PatkaQuizPageModule {}
