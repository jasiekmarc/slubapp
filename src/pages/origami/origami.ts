import { Component } from '@angular/core';

import { IonicPage, NavParams } from 'ionic-angular';
import { GameControllerProvider } from '../../providers/game-controller/game-controller';

@IonicPage()
@Component({
  templateUrl: 'origami.html'
})
export class OrigamiPage {
    key: string;

    constructor(public navParams: NavParams,
                public gameCtrl: GameControllerProvider) {
        this.key = navParams.get('key');
        if (!this.key) {
            gameCtrl.navigateCurrentScreen();
            return;
        }
    }

    doneClicked() {
         this.gameCtrl.navigateNextScreen(this.key);
    }
}

