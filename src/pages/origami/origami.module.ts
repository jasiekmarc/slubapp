import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrigamiPage } from './origami';

@NgModule({
    declarations: [
        OrigamiPage
    ],
    imports: [
        IonicPageModule.forChild(OrigamiPage)
    ],
    entryComponents: [
        OrigamiPage
    ]
})
export class OrigamiPageModule {}
