import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CheckinPage } from './check-in';

@NgModule({
    declarations: [
        CheckinPage
    ],
    imports: [
        IonicPageModule.forChild(CheckinPage)
    ],
    entryComponents: [
        CheckinPage
    ]
})
export class CheckinPageModule {}
