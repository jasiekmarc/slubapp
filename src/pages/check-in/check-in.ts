import { Component } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { IonicPage, NavParams, ToastController } from 'ionic-angular';

import { GameControllerProvider } from "../../providers/game-controller/game-controller";

@IonicPage()
@Component({
    templateUrl: 'check-in.html'
})
export class CheckinPage {
    question: string;
    type: string;  // Expected type of an answer (number/text)
    label: string;  // Label in the form.
    key: string;  // Used to authenticate on form submit.

    checkInForm: FormGroup;

    constructor(public navParams: NavParams, public toastCtrl: ToastController,
                private formBuilder: FormBuilder,
                private gameCtrl: GameControllerProvider) {
        this.question = navParams.get('question');
        this.type = navParams.get('type');
        this.label = navParams.get('label');
        this.key = navParams.get('key');
        let patt = navParams.get('pattern');
        this.checkInForm = this.formBuilder.group({
            ans: ['', Validators.compose([
                        Validators.required,
                        Validators.pattern(patt)])
                ],
        });

        if (!this.key) {
            gameCtrl.navigateCurrentScreen();
        }
    }

    checkIn() {
        if (!this.checkInForm.valid) {
            let toast = this.toastCtrl.create({
                message: 'To nie jest poprawna odpowiedź!',
                duration: 3000,
            });
            toast.present();
        } else {
            this.gameCtrl.navigateNextScreen(this.key);
        }
    }
}