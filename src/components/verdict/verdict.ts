import { Component, Input } from "@angular/core";
import { FormGroup } from "@angular/forms";

@Component({
    selector: 'verdict-card',
    template: `
         <ion-card *ngIf="formGroup.get(form).touched" [class]="(formGroup.get(form).value == correct) ? 'success' : 'danger'">
            <ion-card-header *ngIf="formGroup.get(form).value == correct">
                Dobrze!
            </ion-card-header>
            <ion-card-header *ngIf="formGroup.get(form).value != correct">
                Błąd!
            </ion-card-header>
            <ion-card-content>
                <ng-content></ng-content>
            </ion-card-content>
            <img *ngIf="image" [src]="image"/>
        </ion-card>
    `,
})
export class VerdictCard {
    @Input() formGroup: FormGroup;
    @Input() form: string;
    @Input() correct: string;
    @Input() image: string;


}