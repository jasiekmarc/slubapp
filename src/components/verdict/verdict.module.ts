import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VerdictCard } from "./verdict";

@NgModule({
    declarations: [
        VerdictCard,
    ],
    imports: [
        IonicPageModule.forChild(VerdictCard),
    ],
    exports: [
        VerdictCard,
    ]
})
export class VerdictCardModule {}
