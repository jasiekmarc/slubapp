import { Component, ViewChild } from '@angular/core';

import { Platform, MenuController, Nav } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { StartGamePage } from '../pages/start-game/start-game';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  // make HelloIonicPage the root (or first) page
  rootPage = StartGamePage;
  pages: Array<{title: string, component: any}>;
  secondmenu: Array<{title: string, component: any}>;

  constructor(
    public platform: Platform,
    public menu: MenuController,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public storage: Storage,
  ) {
    this.initializeApp();

    // set our app's pages
    this.pages = [
      { title: 'Start/Kontynuacja', component: StartGamePage },
    ];

    this.secondmenu = this.pages.concat([{
      title: 'Schnapsen',
      component: 'GraKarcianaPage',
    }]);

    storage.get('schnapsenVisited').then(
      (data) => {
        menu.enable(false, 'standard');
        menu.enable(true, 'schnapsen');
      },
    );
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // navigate to the new page if it is not the current page
    this.nav.setRoot(page.component);
  }
}
