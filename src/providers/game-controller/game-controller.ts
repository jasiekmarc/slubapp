import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { App } from 'ionic-angular';

import { StartGamePage } from '../../pages/start-game/start-game';

import { default as graph } from './game-graph';
import { default as pages } from './game-pages';


type Team = "bull" | "goat" | "sheep" | "donkey" | "pig" | "hare" | "dog" |
    "cat" | "duck" | "deer" | "hen" | "cow" | null;

type Point = "pomnikJadwigi" | "laweczkaBanacha" | "muzeumNarodowe" | "wawel" |
    "pomnikPilsudskiego" | "mariacki" | "kosciolBozegoCiala" |
    "staraSynagoga" | "pub" | "start" | "finish" | null;

type ScreenType = "directions" | "checkin" | "quiz" | null;


/* GameControllerProvider is responsible for directing the players through the
points. It will remember, which team the player belongs to and where he has
already reached. It will be responsible for selecting new screens to show. */
@Injectable()
export class GameControllerProvider {

  private _team: Team;
    private _lastPoint: Point;
    private _lastScreen: ScreenType;

    get team(): Team {
        return this._team;
    }
    set team(newTeam: Team) {
        this._team = newTeam;
        this.storageProvider.set('team', newTeam);
        this.lastPoint = null;
        this.lastScreen = null;
    }

    get lastPoint(): Point {
        return this._lastPoint;
    }
    set lastPoint(newLastPoint: Point) {
        this._lastPoint = newLastPoint;
        this.storageProvider.set('lastPoint', newLastPoint);
    }

    get lastScreen(): ScreenType {
        return this._lastScreen;
    }
    set lastScreen(newLastScreen: ScreenType) {
        this._lastScreen = newLastScreen;
        this.storageProvider.set('lastScreen', newLastScreen);
    }

    constructor(private appCtrl: App, private storageProvider: Storage) {
        storageProvider.get('team').then(
            data => this._team = data,
            error => this._team = null,
        );
        storageProvider.get('lastPoint').then(
            data => this._lastPoint = data,
            error => this._lastPoint = null,
        );
        storageProvider.get('lastScreen').then(
            data => this._lastScreen = data,
            error => this._lastScreen = null,
        )
    }

    private navigateToScreen(point: Point, type: ScreenType) {
        this.lastPoint = point;
        this.lastScreen = type;
        console.log(`Controller.navigateToScreen(${point}, ${type}).\n` +
            `LastPoint: ${this.lastPoint}, LastScreen: ${this.lastScreen}`);
        if (pages[point][type].args !== undefined) {
          this.appCtrl.getActiveNav().push(pages[point][type].page,
                                         pages[point][type].args);
          return;
        }
        this.appCtrl.getActiveNav().push(pages[point][type].page);
    }

    private nextScreen(): [Point, ScreenType] {
        if (this.lastScreen === "directions") {
            return [this.lastPoint, "checkin"];
        }
        if (this.lastScreen === "checkin") {
            return [this.lastPoint, "quiz"];
        }
        let lastPoint = this.lastPoint;
        if (lastPoint === null) {
          lastPoint = "start";
        }
        return [graph[this.team][lastPoint], "directions"];
    }

    private getCurrentScreen() {
        if (this.lastPoint == null || this.lastScreen == null) {
            return null;
        }
        return pages[this.lastPoint][this.lastScreen];
    }

    navigateCurrentScreen() {
        console.log(`Controller.navigateCurrentScreen(). Team: ${this.team}, ` +
            `LastPoint: ${this.lastPoint}, LastScreen: ${this.lastScreen}`);
        let curPage = this.getCurrentScreen();
        console.log('curPage: ', curPage);
        if (curPage == null) {
            this.appCtrl.getActiveNav().setPages([{page: StartGamePage}]);
            return;
        }
        if (curPage.args === undefined) {
            this.appCtrl.getActiveNav().setPages([
                {page: StartGamePage},
                {page: curPage.page},
            ]);
            return;
        }
        this.appCtrl.getActiveNav().setPages([
            {page: StartGamePage},
            {page: curPage.page, params: curPage.args},
        ]);
    }

    navigateNextScreen(key?: string) {
        console.log(`Controller.navigateNextScreen(). Team: ${this.team}, ` +
            `LastPoint: ${this.lastPoint}, LastScreen: ${this.lastScreen}`);
        let curPage = this.getCurrentScreen();
        console.log('curPage: ', curPage);
        if (curPage !== null && curPage.args !== undefined &&
            curPage.args.key != key) {
            this.navigateCurrentScreen();
            return;
        }
        let [point, type] = this.nextScreen();
        this.navigateToScreen(point, type);
    }

    started(): boolean {
        return this.lastScreen != null;
    }

}
