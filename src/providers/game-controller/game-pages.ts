export default {
    "pomnikJadwigi" : {
        "directions" : {
            page: "DirectionsPage",
            args: {
                question: "Patrycja wychodząc za mąż niestety nie przyłączy " +
                    "Wilna do Polski. Znajdźcie pomnik pani, której " +
                    "się to udało.",
                hint: "Chodzi tutaj o pomnik Królowej Jadwigi i Króla " +
                    "Władysława Jagiełły na Plantach.",
                key: "dsirxanjwz",
            },
        },
        "checkin" : {
            page: "CheckinPage",
            args: {
                question: "Na pomniku można znaleźć datę jego " +
                        "postawienia. Który jest to rok?",
                type: "number",
                label: "Rok na pomniku",
                pattern: /1886/,
                key: "pezsapnumh",
            }
        },
        "quiz": {
            page: "MathPuzzlesPage",
            args: {
                key: "nsvtlghinc",
            }
        }
    },
    "laweczkaBanacha": {
        "directions": {
            page: "DirectionsPage",
            args: {
                question: "Uczył się w tym samym liceum co Patrycja. Teraz " +
                    "siedzi sobie na ławeczce i dyskutuje o paradoksalnym " +
                    "rozkładzie kuli. Czy znajdziecie miejsce, gdzie " +
                    "przesiaduje?",
                hint: "Chodzi o ławekę na Plantach, na której umieszczono " +
                    "odlane z brązu figury dwóch polskich matematyków.",
                key: "xlklbzjovm",
            }
        },
        "checkin": {
            page: "CheckinPage",
            args: {
                question: "Kto siedzi obok Stefana Banacha?",
                type: "text",
                label: "Imię i nazwisko drugiego matematyka",
                pattern: /Otton Nikodym/i,
                key: "gmgblavfbm",
            },
        },
        "quiz": {
            page: "MathPuzzlesPage",
            args: {
                key: "nsvtlghinc",
            }
        }
    },
    "muzeumNarodowe": {
        "directions": {
            page: "DirectionsPage",
            args: {
                question: "W punkcie, do którego macie się teraz udać " +
                    "Patrycja bawiła się tam na balu studniówkowym. " +
                    "Teraz bawi tam inna inna pani, wraz ze swoją łasicą.",
                hint: "Chodzi o Muzeum Narodowe.",
                key: "drhasfqjgd",
            },
        },
        "checkin": {
            page: "CheckinPage",
            args: {
                question: "Czyj pomnik stoi przed budynkiem muzeum?",
                type: "text",
                label: "Imię i nazwisko człowieka na pomniku.",
                pattern: /Stanis[lł]aw Wyspia[nń]ski/i,
                key: "lxkunabgth",
            }
        },
        "quiz": {
            page: "MuzeumQuizPage",
            args: {
                key: "pubrttbtdl",
            }
        }
    },
    "wawel": {
        "directions": {
            page: "DirectionsPage",
            args: {
                question: "Pójdźcie na wzgórze, na którym bije serce Polski.",
                hint: "Tu wskazówka nie powinna być potrzebna. Chodzi " +
                    "oczywiście o Wawel.",
                key: "yceuhaziom",
            },
        },
        "checkin": {
            page: "CheckinPage",
            args: {
                question: "Która atrakcja na Wawelu jest oznaczona na " +
                    "tablicy informacyjnej piktogramem węża strażackiego?",
                type: "text",
                label: "Opis na legendzie na tablicy informacyjnej",
                pattern: /Muzeum Katedralne/i,
                key: "oovwjswetk",
            }
        },
        "quiz": {
            page: "PatkaQuizPage",
            args: {
                key: "srgtajnkns",
            },
        },
    },
    "pomnikPilsudskiego": {
        "directions": {
            page: "DirectionsPage",
            args: {
                question: "Jego serce jest w Wilnie, a w biurze Parycji wisi " +
                    "jego portret na Kasztance. Czy znajdziecie jego pomnik?",
                hint: "Pomnik Piłsudskiego jest przy ulicy Piłsudskiego.",
                key: "oovwjswetk",
            }
        },
        "checkin": {
            page: "CheckinPage",
            args: {
                question: "Jak brzmi druga linijka pod ośmioma butami?",
                type: "text",
                label: "Tekst z pomnika",
                pattern: /Legiony to ?[-–—]? ?ofiarny stos/i,
                key: "cfanklinqy",
            }
        },
        "quiz": {
            page: "JasiekQuizPage",
            args: {
                key: "nsvtlghinc",
            }
        },
    },
    "mariacki" : {
        "directions": {
            page: "DirectionsPage",
            args: {
                question: "Przez zawiść jednego brata jej wieże są nierówne." +
                    "Podejdźcie pod ten znany budynek",
                hint: "Z wieży tego kościoła grają co godzinę hejnał.",
                key: "gymomxuadi",
            }
        },
        "checkin": {
            page: "CheckinPage",
            args: {
                question: "Pod jakim wezwaniem jest pierwsza kaplica po " +
                    "prawej od wejścia do świątyni (Matki Bożej " +
                    "Częstochowskiej)?",
                type: "text",
                label: "Wezwanie (dopełniacz)",
                pattern: /Matki Boskiej Cz[eę]stochowskiej/i,
                key: "weurhnltkj",
            },
        },
        "quiz": {
            page: "JasiekQuizPage",
            args: {
                key: "zjgdhzneyn",
            }
        }
    },
    "kosciolBozegoCiala": {
        "directions": {
            page: "DirectionsPage",
            args: {
                question: "Przedwczoraj był w nim odpust. Ufundował go " +
                    "Kazimierz Wielki. Czy znajdziecie ten kościół?",
                hint: "Szukajcie na Kazimierzu i pamiętajcie, jakie święto " +
                    "było przedwczoraj.",
                key: "mrcylvnkjs",
            }
        },
        "checkin": {
            page: "CheckinPage",
            args: {
                question: "Z którego wieku jest ołtarz główny?",
                type: "number",
                label: "Stulecie powstania ołtarza",
                pattern: /17/,
                key: "lgmxwwtaaa",
            }
        },
        "quiz" : {
            page: "OrigamiPage",
            args: {
                key: "lrfkwhgjkd",
            }
        },
    },
    "staraSynagoga": {
        "directions": {
            page: "DirectionsPage",
            args: {
                question: "Gdyby Jasiek był starozakonnym przed wojną, tam " +
                    "brałby ślub. Dziś jest w środku muzeum. Traficie do " +
                    "tego miejsca?",
                hint: "Szukamy Starej Synagogi na Kazimierzu.",
                key: "gyxrmjtaje",
            }
        },
        "checkin": {
            page: "CheckinPage",
            args: {
                question: "Stancie na placu twarzą do Synagogi. Po lewej " +
                    "stronie jest wielki szyld. Co jest na nim napisane?",
                type: "text",
                label: "Nazwa z szyldu",
                pattern: /brandman/i,
                key: "xfgxnohtnw",
            }
        },
        "quiz" : {
            page: "OrigamiPage",
            args: {
                key: "adncqqlwbo",
            }
        },
    },
    "pub": {
        "directions": {
            page: "DirectionsPage",
            args: {
                question: "Przy ulicy Józefa na Kazimierzu znajdziecie bar " +
                    "o nazwie Bill Hickman",
                hint: "Dokładny adres to Józefa 11",
                key: "vlpcrskyxa",
            }
        },
        "checkin": {
            page: "CheckinPage",
            args: {
                question: "Jeśli wręczycie barmanowi (barmance) wasze " +
                    "Origami, powinniście dostać coś do picia. Polecamy " +
                    "„Stumbrasa”, ale możecie wybrać cokolwiek z tej samej " +
                    "kategorii, espresso lub sok. Powinniście również " +
                    "usłyszeć hasło.",
                type: "text",
                label: "Hasło od barmana",
                pattern: /krupnik/i,
                key: "eahbnzvikq",
            }
        },
        "quiz": {
            page: "GraKarcianaPage",
            args: {
                key: "zqgectzjzy",
            }
        },
    },
    "finish": {
        "directions": {
            page: "KoniecPage",
        },
    }
}
