module.exports = {
  "globDirectory": "www\\",
  "globPatterns": [
    "**/*.{woff2,ico,jpg,png,js,css,html,json}"
  ],
  "swDest": "www/service-worker.js",
  "globIgnores": [
    "..\\workbox-cli-config.js"
  ]
};
