This web-app was used in an Urban Game after our wedding in 2017.
The app is hosted at
[https://slub-gra.herokuapp.com/](https://slub-gra.herokuapp.com/).

## The Urban Game
Players are divided into six teams (named after farm animals). The teams walk
together around the city and visit different points, as the app leads them.

### Screens
Every landmark in the city is associated with three screens:

- A directions screen giving the players hints on how to reach the point.
- A check-in screen with an easy question verifying that they indeed are at the
  right place.
- A task screen that gives them an additional quiz to solve or a task to
  perform. It doesn't need to relate to the landmark. An example screen is
  `patka-quiz`, a funny quiz about the bride.

### Start
Upon a start of the game a password is provided to the players. They were
supposed to install the app beforehand, but were not able to play without the
password.


## Progressive Web App
Since not everyone has a mobile Internet connection, it was necessary, that the
app works offline after it is successfully downloaded. A service worker is used
to achieve that. We decided to use Google-produced
[Workbox](https://workboxjs.org/) toolbox library to simplify caching.